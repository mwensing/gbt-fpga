-------------------------------------------------------
--! @file
--! @author Julian Mendez <julian.mendez@cern.ch> (CERN - EP-ESE-BE)
--! @version 6.0
--! @brief GBT-FPGA IP - Transceiver Tx PLL
-------------------------------------------------------

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Altera devices library:
library altera; 
library altera_mf;
library lpm;
use altera.altera_primitives_components.all;   
use altera_mf.altera_mf_components.all;
use lpm.lpm_components.all;

--! Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;

--! Libraries for direct instantiation:
library mgt_atxpll;
library mgt_tx_pll_rst;

--! @brief ALT_ax_mgt_txpll - Transceiver Tx PLL
--! @details 
--! The ALT_ax_mgt_txpll provides the serial clock to the transmitter
entity alt_ax_mgt_txpll is
   port (      
      
      --=======--  
      -- Reset --  
      --=======--  
      RESET_I                                   : in  std_logic;
      
      --===============--  
      -- Clocks scheme --  
      --===============--        
      MGT_REFCLK_I                              : in  std_logic;
      TX_BONDING_CLK_O									: out std_logic_vector(5 downto 0);
		      
      --=========--
      -- Control --
      --=========--      
      LOCKED_O                                  : out std_logic;
		
		--============--
		-- Reconfig.  --
		--============--
		tx_pll_reconfig_write       					: in  std_logic                     := '0';             --    reconfig_avmm0.write
		tx_pll_reconfig_read        					: in  std_logic                     := '0';             --                  .read
		tx_pll_reconfig_address     					: in  std_logic_vector(9 downto 0)  := (others => '0'); --                  .address
		tx_pll_reconfig_writedata   					: in  std_logic_vector(31 downto 0) := (others => '0'); --                  .writedata
		tx_pll_reconfig_readdata    					: out std_logic_vector(31 downto 0);                    --                  .readdata
		tx_pll_reconfig_waitrequest 					: out std_logic;                                        --                  .waitrequest
		tx_pll_reconfig_clk         					: in  std_logic                     := '0';             --     reconfig_clk0.clk
		tx_pll_reconfig_reset       					: in  std_logic                     := '0'              --   reconfig_reset0.reset

   );
end alt_ax_mgt_txpll;

--! @brief ALT_ax_mgt_txpll architecture - Transceiver Tx PLL
--! @details The ALT_ax_mgt_txpll implement the transmitter pll (ATXPLL or FPLL depending on user configuration)
architecture structural of alt_ax_mgt_txpll is 

   --================================ Signal Declarations ================================--
   signal txpll_powerdown:	std_logic;
	
	component mgt_fpll is
		port (
			mcgb_rst          : in  std_logic                    := 'X'; -- mcgb_rst
			--mcgb_serial_clk   : out std_logic;                           -- clk
			pll_cal_busy      : out std_logic;                           -- pll_cal_busy
			pll_locked        : out std_logic;                           -- pll_locked
			pll_powerdown     : in  std_logic                    := 'X'; -- pll_powerdown
			pll_refclk0       : in  std_logic                    := 'X'; -- clk
			tx_bonding_clocks : out std_logic_vector(5 downto 0);        -- clk
			tx_serial_clk     : out std_logic;                           -- clk
			
			reconfig_write0       : in  std_logic                     := '0';             --    reconfig_avmm0.write
			reconfig_read0        : in  std_logic                     := '0';             --                  .read
			reconfig_address0     : in  std_logic_vector(9 downto 0)  := (others => '0'); --                  .address
			reconfig_writedata0   : in  std_logic_vector(31 downto 0) := (others => '0'); --                  .writedata
			reconfig_readdata0    : out std_logic_vector(31 downto 0);                    --                  .readdata
			reconfig_waitrequest0 : out std_logic;                                        --                  .waitrequest
			reconfig_clk0         : in  std_logic                     := '0';             --     reconfig_clk0.clk
			reconfig_reset0       : in  std_logic                     := '0'              --   reconfig_reset0.reset
		);
	end component mgt_fpll;

   --=====================================================================================--   
   
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
   
   --==================================== User Logic =====================================-- 
   
	--=============--
	-- GTX PLL Rst --
	--=============--
	transceiver_atxpll_rst: entity mgt_tx_pll_rst.mgt_tx_pll_rst
		port map (
			clock         		=> MGT_REFCLK_I,
			pll_powerdown(0)	=> txpll_powerdown,
			reset         		=> RESET_I
		);
		
   --===========--
   -- GX TX PLL --
   --===========--
	
   fpll_gen: if XCVR_TX_PLL = FPLL generate
	   transceiver_mgtpll: mgt_fpll
			port map(
				mcgb_rst          	=> txpll_powerdown,			--          mcgb_rst.mcgb_rst
				--mcgb_serial_clk   	=> open,                   -- clk
				--pll_cal_busy      	=> open,                   --      pll_cal_busy.pll_cal_busy
				pll_locked        	=> LOCKED_O,					--        pll_locked.pll_locked
				pll_powerdown     	=> txpll_powerdown,			--     pll_powerdown.pll_powerdown
				pll_refclk0       	=> MGT_REFCLK_I,				--       pll_refclk0.clk
				tx_bonding_clocks 	=> TX_BONDING_CLK_O,       -- tx_bonding_clocks.clk
				tx_serial_clk     	=> open, 							--     tx_serial_clk.clk
				
				reconfig_write0       => tx_pll_reconfig_write,       --    reconfig_avmm0.write
				reconfig_read0        => tx_pll_reconfig_read,        --                  .read
				reconfig_address0     => tx_pll_reconfig_address,     --                  .address
				reconfig_writedata0   => tx_pll_reconfig_writedata,   --                  .writedata
				reconfig_readdata0    => tx_pll_reconfig_readdata,    --                  .readdata
				reconfig_waitrequest0 => tx_pll_reconfig_waitrequest, --                  .waitrequest
				reconfig_clk0         => tx_pll_reconfig_clk,         --     reconfig_clk0.clk
				reconfig_reset0       => tx_pll_reconfig_reset        --   reconfig_reset0.reset
			);
   end generate;
	
   ATXpll_gen: if XCVR_TX_PLL = ATXPLL generate
	   transceiver_atxpll: entity mgt_atxpll.mgt_atxpll
			port map(
				mcgb_rst          	=> txpll_powerdown,			--          mcgb_rst.mcgb_rst
				--mcgb_serial_clk   	=> open,                   -- clk
				--pll_cal_busy      	=> open,                   --      pll_cal_busy.pll_cal_busy
				pll_locked        	=> LOCKED_O,					--        pll_locked.pll_locked
				pll_powerdown     	=> txpll_powerdown,			--     pll_powerdown.pll_powerdown
				pll_refclk0       	=> MGT_REFCLK_I,				--       pll_refclk0.clk
				tx_bonding_clocks 	=> TX_BONDING_CLK_O,       -- tx_bonding_clocks.clk
				tx_serial_clk     	=> open, 							--     tx_serial_clk.clk
				
				reconfig_write0       => tx_pll_reconfig_write,       --    reconfig_avmm0.write
				reconfig_read0        => tx_pll_reconfig_read,        --                  .read
				reconfig_address0     => tx_pll_reconfig_address,     --                  .address
				reconfig_writedata0   => tx_pll_reconfig_writedata,   --                  .writedata
				reconfig_readdata0    => tx_pll_reconfig_readdata,    --                  .readdata
				reconfig_waitrequest0 => tx_pll_reconfig_waitrequest, --                  .waitrequest
				reconfig_clk0         => tx_pll_reconfig_clk,         --     reconfig_clk0.clk
				reconfig_reset0       => tx_pll_reconfig_reset        --   reconfig_reset0.reset
			);
   end generate;
   
   --=====================================================================================--   
end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--